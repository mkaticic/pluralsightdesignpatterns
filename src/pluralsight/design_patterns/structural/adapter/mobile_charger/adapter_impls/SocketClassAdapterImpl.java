package pluralsight.design_patterns.structural.adapter.mobile_charger.adapter_impls;

import pluralsight.design_patterns.structural.adapter.mobile_charger.basic.Socket;
import pluralsight.design_patterns.structural.adapter.mobile_charger.basic.SocketAdapter;
import pluralsight.design_patterns.structural.adapter.mobile_charger.basic.Volt;

public class SocketClassAdapterImpl extends Socket implements SocketAdapter{
	 
    @Override
    public Volt get120Volt() {
        return getVolt();
    }
 
    @Override
    public Volt get12Volt() {
        Volt v= getVolt();
        return convertVolt(v,10);
    }
 
    @Override
    public Volt get3Volt() {
        Volt v= getVolt();
        return convertVolt(v,40);
    }
     
    private Volt convertVolt(Volt v, int i) {
        return new Volt(v.getVolts()/i);
    }
 
}
