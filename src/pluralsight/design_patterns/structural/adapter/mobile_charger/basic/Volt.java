package pluralsight.design_patterns.structural.adapter.mobile_charger.basic;

public class Volt {
    private int volts;
     
    public Volt(int v){
        this.volts=v;
    }
 
    public int getVolts() {
        return volts;
    }
 
    public void setVolts(int volts) {
        this.volts = volts;
    }    
}
