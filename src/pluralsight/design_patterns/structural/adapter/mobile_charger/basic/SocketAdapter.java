package pluralsight.design_patterns.structural.adapter.mobile_charger.basic;


public interface SocketAdapter {	 
    public Volt get120Volt();
         
    public Volt get12Volt();
     
    public Volt get3Volt();
}
