package pluralsight.design_patterns.structural.adapter.mobile_charger.basic;


public class Socket {		 
    public Volt getVolt(){
        return new Volt(120);
    }
}
