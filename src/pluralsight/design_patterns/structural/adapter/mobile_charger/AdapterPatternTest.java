package pluralsight.design_patterns.structural.adapter.mobile_charger;

import pluralsight.design_patterns.structural.adapter.mobile_charger.adapter_impls.SocketClassAdapterImpl;
import pluralsight.design_patterns.structural.adapter.mobile_charger.adapter_impls.SocketObjectAdapterImpl;
import pluralsight.design_patterns.structural.adapter.mobile_charger.basic.SocketAdapter;
import pluralsight.design_patterns.structural.adapter.mobile_charger.basic.Volt;

/**
 * Adapter design pattern is one of the <b>structural</b> design pattern and its used so that two unrelated interfaces can work together. The object that joins these unrelated interface is called an Adapter. As a real life example, we can think of a mobile charger as an adapter because mobile battery needs 3 volts to charge but the normal socket produces either 120V (US) or 240V (India). So the mobile charger works as an adapter between mobile charging socket and the wall socket.
 * We will try to implement multi-adapter using adapter design pattern in this tutorial.
 * So first of all we will have two classes – Volt (to measure volts) and Socket (producing constant volts of 120V).
 * <br><br>
 * While implementing Adapter pattern, there are <i>two approaches</i> – class adapter and object adapter, however both these approaches produce same result.
    <li><b>Class Adapter</b> – This form uses java inheritance and extends the source interface, in our case Socket class.
    <li><b>Object Adapter</b> – This form uses Java Composition and adapter contains the source object.
 * 
 */
public class AdapterPatternTest {
	 
    public static void main(String[] args) {
         
        testClassAdapter();
        testObjectAdapter();
    }
 
    private static void testObjectAdapter() {
        SocketAdapter sockAdapter = new SocketObjectAdapterImpl();
        Volt v3 = getVolt(sockAdapter,3);
        Volt v12 = getVolt(sockAdapter,12);
        Volt v120 = getVolt(sockAdapter,120);
        System.out.println("v3 volts using Object Adapter="+v3.getVolts());
        System.out.println("v12 volts using Object Adapter="+v12.getVolts());
        System.out.println("v120 volts using Object Adapter="+v120.getVolts());
    }
 
    private static void testClassAdapter() {
        SocketAdapter sockAdapter = new SocketClassAdapterImpl();
        Volt v3 = getVolt(sockAdapter,3);
        Volt v12 = getVolt(sockAdapter,12);
        Volt v120 = getVolt(sockAdapter,120);
        System.out.println("v3 volts using Class Adapter="+v3.getVolts());
        System.out.println("v12 volts using Class Adapter="+v12.getVolts());
        System.out.println("v120 volts using Class Adapter="+v120.getVolts());
    }
     
    private static Volt getVolt(SocketAdapter sockAdapter, int i) {
        switch (i){
	        case 3: return sockAdapter.get3Volt();
	        case 12: return sockAdapter.get12Volt();
	        case 120: return sockAdapter.get120Volt();
	        default: return sockAdapter.get120Volt();
        }
    }
}
