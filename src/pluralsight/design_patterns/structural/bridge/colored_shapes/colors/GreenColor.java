package pluralsight.design_patterns.structural.bridge.colored_shapes.colors;

import pluralsight.design_patterns.structural.bridge.colored_shapes.basic.Color;

public class GreenColor implements Color{
	 
    public void applyColor(){
        System.out.println("green.");
    }
}
