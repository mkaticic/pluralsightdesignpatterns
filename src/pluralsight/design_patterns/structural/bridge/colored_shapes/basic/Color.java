package pluralsight.design_patterns.structural.bridge.colored_shapes.basic;

public interface Color {	 
    public void applyColor();
}
