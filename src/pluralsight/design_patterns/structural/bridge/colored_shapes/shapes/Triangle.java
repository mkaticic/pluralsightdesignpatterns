package pluralsight.design_patterns.structural.bridge.colored_shapes.shapes;



import pluralsight.design_patterns.structural.bridge.colored_shapes.basic.Color;
import pluralsight.design_patterns.structural.bridge.colored_shapes.basic.Shape;

public class Triangle extends Shape{
	 
    public Triangle(Color c) {
        super(c);
    }
 
    @Override
    public void applyColor() {
        System.out.print("Triangle filled with color ");
        color.applyColor();
    }
 
}
