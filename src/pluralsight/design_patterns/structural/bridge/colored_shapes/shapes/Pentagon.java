package pluralsight.design_patterns.structural.bridge.colored_shapes.shapes;

import pluralsight.design_patterns.structural.bridge.colored_shapes.basic.Color;
import pluralsight.design_patterns.structural.bridge.colored_shapes.basic.Shape;

public class Pentagon extends Shape{
	 
    public Pentagon(Color c) {
        super(c);
    }
 
    @Override
    public void applyColor() {
        System.out.print("Pentagon filled with color ");
        color.applyColor();
    }
 
}
