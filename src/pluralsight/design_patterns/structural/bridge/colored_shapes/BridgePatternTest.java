package pluralsight.design_patterns.structural.bridge.colored_shapes;

import pluralsight.design_patterns.structural.bridge.colored_shapes.basic.Shape;
import pluralsight.design_patterns.structural.bridge.colored_shapes.colors.GreenColor;
import pluralsight.design_patterns.structural.bridge.colored_shapes.colors.RedColor;
import pluralsight.design_patterns.structural.bridge.colored_shapes.shapes.Pentagon;
import pluralsight.design_patterns.structural.bridge.colored_shapes.shapes.Triangle;

public class BridgePatternTest {
	 
    public static void main(String[] args) {
        Shape tri = new Triangle(new RedColor());
        tri.applyColor();
         
        Shape pent = new Pentagon(new GreenColor());
        pent.applyColor();
    }
 
}
