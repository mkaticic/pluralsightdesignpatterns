package pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic;

public enum ApprovalResponse {
	approved,
	denied,
	beyondAppLimit;
}
