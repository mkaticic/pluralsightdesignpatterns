package pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic;

public class ExpenseHandler implements IExpenseHandler {
	IExpenseApprover approver;
	IExpenseHandler next;
	
	public ExpenseHandler(IExpenseApprover approver) {
		this.approver = approver;
		this.next = EndOfChainHandler.getInstance();
	}
	
	@Override
	public void registerNext(IExpenseHandler next) {
		this.next = next;
		
	}

	@Override
	public ApprovalResponse approve(IExpeseReport exp) {
		System.out.println(approver.getName() + " is approving...");
		ApprovalResponse res = approver.approve(exp);
		if(res == ApprovalResponse.beyondAppLimit){
			return next.approve(exp);
		}
		return res;
	}

}
