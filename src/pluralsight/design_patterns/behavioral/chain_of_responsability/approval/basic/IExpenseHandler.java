package pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic;

public interface IExpenseHandler {

	public abstract void registerNext(IExpenseHandler tatjana);

	public abstract ApprovalResponse approve(IExpeseReport exp);

}