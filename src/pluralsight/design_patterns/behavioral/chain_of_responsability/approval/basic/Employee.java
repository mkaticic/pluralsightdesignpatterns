package pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic;

import java.math.BigDecimal;

public class Employee implements IExpenseApprover{
	String name;
	BigDecimal approvalLimit;
	
	public Employee(String name, BigDecimal amount) {
		this.name = name;
		this.approvalLimit = amount;
	}
	
	@Override
	public ApprovalResponse approve(IExpeseReport exp) {
		if(exp.total().compareTo(approvalLimit)>0){
			return ApprovalResponse.beyondAppLimit;
		}
		System.out.print(name + ": ");
		return ApprovalResponse.approved;
	}

	@Override
	public String getName() {
		return name;
	}
	
	
}
