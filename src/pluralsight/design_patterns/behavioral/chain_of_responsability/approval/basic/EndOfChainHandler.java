package pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic;

public class EndOfChainHandler implements IExpenseHandler{
	public static EndOfChainHandler getInstance(){
		return instance;
	}
	
	@Override
	public void registerNext(IExpenseHandler next) {
		throw new RuntimeException("iNVALID OPERATION");
	}

	@Override
	public ApprovalResponse approve(IExpeseReport exp) {
		return ApprovalResponse.denied;
	}
	
	private static final EndOfChainHandler instance = new EndOfChainHandler();
}
