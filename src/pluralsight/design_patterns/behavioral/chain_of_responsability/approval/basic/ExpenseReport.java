package pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic;

import java.math.BigDecimal;

public class ExpenseReport implements IExpeseReport {

	BigDecimal expenseReportAmount;
	public ExpenseReport(BigDecimal expenseReportAmount) {
		this.expenseReportAmount = expenseReportAmount;
	}
	
	@Override
	public BigDecimal total() {
		return expenseReportAmount;
	}

}
