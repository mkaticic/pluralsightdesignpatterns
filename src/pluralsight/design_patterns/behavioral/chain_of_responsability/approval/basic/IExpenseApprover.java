package pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic;

public interface IExpenseApprover {
	public abstract ApprovalResponse approve(IExpeseReport exp);

	public abstract String getName();
}
