package pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic;

import java.math.BigDecimal;

public interface IExpeseReport {
	public BigDecimal total();
}
