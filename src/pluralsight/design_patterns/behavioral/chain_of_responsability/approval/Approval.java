package pluralsight.design_patterns.behavioral.chain_of_responsability.approval;

import java.math.BigDecimal;

import pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic.ApprovalResponse;
import pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic.Employee;
import pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic.ExpenseHandler;
import pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic.ExpenseReport;
import pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic.IExpenseHandler;
import pluralsight.design_patterns.behavioral.chain_of_responsability.approval.basic.IExpeseReport;

public class Approval {
	public static void main(String[] args){
		IExpenseHandler mirna = new ExpenseHandler(new Employee("mirna",new BigDecimal(1000)));
		IExpenseHandler tatjana = new ExpenseHandler(new Employee("tatjana",new BigDecimal(5000)));
		IExpenseHandler botunic = new ExpenseHandler(new Employee("botunic",new BigDecimal(10000)));
		IExpenseHandler boro = new ExpenseHandler(new Employee("boro",new BigDecimal(100000)));
		
		mirna.registerNext(tatjana);
		tatjana.registerNext(botunic);
		botunic.registerNext(boro);
		
		BigDecimal expenseReportAmount = new BigDecimal(110);
		IExpeseReport exp = new ExpenseReport(expenseReportAmount);
		ApprovalResponse res = mirna.approve(exp);
		
		System.out.println(res);
		
	}
}
